import {combineReducers} from "redux";
import items from './itemsReducer';
import cart from './cartReducer';
const rootReducer = combineReducers({
  items,
  cart
});

export default rootReducer;
