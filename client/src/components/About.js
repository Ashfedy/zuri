import React from 'react';
import {Link} from 'react-router';

const About = () => {
    return (
      <div>
        <h1>About</h1>
        <p>About Page</p>
        <Link to="/" className="btn btn-primary btn-lg">Home</Link>
      </div>
    );
};
export default About;

// class About extends React.Component {
//   render() {
//     return (
//       <div>
//         <h1>About</h1>
//         <p>This application uses React, Redux, React Router and a variety of other helpful libraries.</p>
//       </div>
//     );
//   }
// }

// export default About;
