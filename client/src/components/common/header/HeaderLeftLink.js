import React,{PropTypes} from 'react';
import {Link} from 'react-router';

const HeaderLeftLink = ({activateBackground}) => {
  return (
    <ul className="nav navbar-nav">
      <li className="active home-link"><Link to="/" onClick={activateBackground}>Home <span className="sr-only">(current)</span></Link></li>
      <li className="gallery-link"><Link to="/gallery" onClick={activateBackground}>Gallery</Link></li>
      <li className="contact-link"><Link to="/contact" onClick={activateBackground}>Contact Us</Link></li>
    </ul>
  );
};
HeaderLeftLink.propTypes = {
  activateBackground: PropTypes.func.isRequired
};

export default HeaderLeftLink;
