import React from 'react';

const Home = () => {
    return (
      <div className="homepage-wrapper">
        <div className="first-fold">
          <div className="container-fluid">
            <h1 className="text-center homepage-title">Z<span className="homepage-title-pink">UR</span>I</h1>
            <h3 className="homepage-description">Zuri chennai for trendy and classy fashion jewellery at the best quality and best price.</h3>
            </div>
          </div>
      </div>
    );
};
//  Leave a comment or Whatsapp at <a href="tel:07338876655">07338876655</a> to place your orders and for enquiries.

export default Home;
