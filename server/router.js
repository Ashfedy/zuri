import itemController from './controllers/itemController';
import cartController from './controllers/cartController';
const router = (app) => { //(app) add "app" as parameter  to the router function
	// console.log('in the router....');
	app.get('/', itemController.getAllItems);
	app.get('/fetchCart', cartController.getAllItems);
	// app.post('/signups', authFunc.signup);
	// app.post('/mentorsignup', authFunc.mentorsignup);
	// app.post('/activateMentor', authFunc.activateMentor);
	// app.post('/sverifyemail', authFunc.verifyEmail);
	// app.post('/signin', requireSignin, authFunc.signin);
};

export default router;
