import mongoose from 'mongoose';

export default function db_connection() {
  const mongoDB = 'mongodb://zuri_admin:password@ds155473.mlab.com:55473/ashfedy_aws';
  mongoose.connect(mongoDB, {
    useMongoClient: true
  });
  const db = mongoose.connection;
  return db;
}
