import express from 'express';
import cors from 'cors';
import bodyParser from 'body-parser';
import router from './router';
import db_connection from './controllers/dbConnection';

const app = express();

//Set up mongoose connection
const db = db_connection();
db.on('error', console.error.bind(console, 'MongoDB connection error:'));
// console.log(db.gallery.find());
app.use(cors({
origin: ["http://localhost:4000"],
methods: ["GET", "POST", "OPTIONS"],
allowedHeaders: ["Content-Type", "Authorization"]
}));

app.use(bodyParser.urlencoded({     // to support URL-encoded bodies
  extended: true
}));

//app.use( bodyParser.json() );       // to support JSON-encoded bodies
app.use(bodyParser.json({ type: '*/*' }));

router(app);

app.set("port", process.env.PORT || 4001);

// Express only serves static assets in production
if (process.env.NODE_ENV === "production") {
  app.use(express.static("client/build"));
}

app.listen(app.get("port"), () => {
  console.log(`Find the server at: http://localhost:${app.get("port")}/`); // eslint-disable-line no-console
});
